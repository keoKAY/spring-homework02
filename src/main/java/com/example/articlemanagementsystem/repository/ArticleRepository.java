package com.example.articlemanagementsystem.repository;

import com.example.articlemanagementsystem.model.Article;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;



@Repository
public class ArticleRepository {

    List<Article> articleList = new ArrayList<>();

    public ArticleRepository(){
        Faker fakObj = new Faker();

        for (int i=1 ; i<= 10; i++) {

            Article article = new Article();
            article.setId(i);
            article.setTitle(fakObj.artist().name());
            article.setDescription(fakObj.harryPotter().quote());
            article.setImageUrl(fakObj.name().fullName());


            articleList.add(article);
        }
    }



    //

    public List<Article> getAllArticle(){

        return articleList;
    }
}
