package com.example.articlemanagementsystem.controller;

import com.example.articlemanagementsystem.service.ArticleService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class ArticleController {

 ArticleService articleService;

public ArticleController( ArticleService articleService){

        this.articleService = articleService;
}


@GetMapping("")
public String viewIndex(ModelMap modelMap){


    modelMap.addAttribute("articleList",articleService.getAll());
    return "index";
}

}
