package com.example.articlemanagementsystem.service.imp;

import com.example.articlemanagementsystem.model.Article;
import com.example.articlemanagementsystem.repository.ArticleRepository;
import com.example.articlemanagementsystem.service.ArticleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {
    ArticleRepository articleRepository;

    public ArticleServiceImp ( ArticleRepository articleRepository ){
            this.articleRepository =  articleRepository;
    }

    @Override
    public List<Article> getAll() {
        return articleRepository.getAllArticle();
    }
}
