package com.example.articlemanagementsystem.service;

import com.example.articlemanagementsystem.model.Article;

import java.util.List;

public interface ArticleService {


    public List<Article>  getAll();

}
